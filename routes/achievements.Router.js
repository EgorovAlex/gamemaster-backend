const Router = require('express');
const router = new Router();
const authMiddleware = require('../middleware/authMiddleware');
const achievementsController = require("../controllers/achievements.Contoller");

router.get('/killer/userAchievements', authMiddleware, achievementsController.getUserAchievements);

router.get('/killer/closedAchievements', authMiddleware, achievementsController.getClosedAchievements);



module.exports = router;