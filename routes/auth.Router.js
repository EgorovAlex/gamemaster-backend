const Router = require('express');
const router = new Router();
const {check} = require('express-validator');
const authMiddleware = require('../middleware/authMiddleware');

const authController = require("../controllers/auth.Controller");

router.post('/auth/registration', [check('login', "Имя пользователя не может быть пустым").notEmpty(),
            check("password", "Некорректная длина пароля").isLength({min: 4, max: 20})], authController.register);

router.post('/auth/login', authController.login);

router.get('/auth/users', authMiddleware, authController.users);

router.get('/auth/userInfo', authMiddleware, authController.userInfo);


module.exports = router;