const Router = require('express');
const router = new Router();
const authMiddleware = require('../middleware/authMiddleware');
const lobbieController = require("../controllers/lobbies.Controller");


router.post('/lobbies', authMiddleware, lobbieController.postLobbie);

router.get('/lobbies', authMiddleware, lobbieController.lobbies);

router.put('/lobbies/entrance', authMiddleware, lobbieController.entrance);

router.get('/lobbies/killer/members', authMiddleware, lobbieController.killerMembers);

router.put('/lobbies/killer/start', authMiddleware, lobbieController.startKiller);

router.put('/lobbies/killer/died', authMiddleware, lobbieController.died);

router.delete('/lobbies/killer/leave', authMiddleware, lobbieController.killerLeave);

router.put('/lobbies/killer/winners', authMiddleware, lobbieController.killerWinners);

router.delete('/lobbies/killer/lobbies', authMiddleware, lobbieController.deleteLobby);






module.exports = router;