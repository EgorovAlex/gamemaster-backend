const db = require('../db');
const {validationResult} = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {secret} = require('../config');
const levelSystem = require('../utils/levelSystem');


const generateAccessToken = (userID, killerID) => {
    const payload =  {userID, killerID};
    return jwt.sign(payload, secret);
}

class AuthController {
    async register(req, res) {
        try {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(400).json({message: "Некорректный логин или пароль", errors});
          }
          const user = req.body;
          const candidateLogin = await db.query(`SELECT login FROM public."User" WHERE login = '${user.login}'`);
          const candidateEmail = await db.query(`SELECT email FROM public."User" WHERE email = '${user.email}'`);

          if (candidateLogin.rowCount != 0) {
             return res.status(401).json({message: "Пользователь уже зарегистрирован"});
          }

          if (candidateEmail.rowCount != 0) {
             return res.status(401).json({message: "Пользователь с данной почтой уже зарегистрирован"});
          }
            
          const hashedPassword = bcrypt.hashSync(user.password, 4);
          
          let registrationDate =  new Date().toJSON();

        
          console.log(registrationDate);
          const userID = (await db.query(`INSERT INTO public."User" (login, email, password, registration, avatar) VALUES ('${user.login}', '${user.email}',
               '${hashedPassword}','${registrationDate}', '${user.avatar}')  RETURNING "UserID"`)).rows[0].UserID;

          const killerUserID =  (await db.query(`INSERT INTO public."KillerUser" ("UserID") VALUES ('${userID}') RETURNING "KillerUserID"`)).rows[0].KillerUserID;

          const token = generateAccessToken(userID, killerUserID);

          return res.status(201).json({token});
        }
        catch(e) {
            console.log(e);
            res.status(400).json({message : "Registration error"}); 
        }

    }
    
    async login(req, res) {

        try {
            const user = req.body;
            const candidate = await db.query(`SELECT "UserID", login, password FROM public."User" WHERE login = '${user.login}'`);

            if (candidate.rowCount == 0) {
                return res.status(401).json({message: "Пользователь не найден"});
            }
            const isPassValid = bcrypt.compareSync(user.password, candidate.rows[0].password);
            if (!isPassValid) {
                return res.status(401).json({message: "Неверный пароль"});
            }
            const  killerID = (await db.query(`SELECT "KillerUserID" FROM public."KillerUser" WHERE "UserID" = ${candidate.rows[0].UserID}`)).rows[0].KillerUserID;
            const token = generateAccessToken(candidate.rows[0].UserID, killerID);

            return res.status(201).json({token});
        }
        catch(e) {
            console.log(e);
            res.status(400).json({message : "login error"});
        }

    }

    async users(req, res) {

        try {
            const users = await db.query(`SELECT * FROM public."User"`);
            console.log(req.user);
            return res.status(200).json(users.rows);
        }
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }

    }

    async userInfo(req, res) {
        try {
            let killerID = req.query.killerID;
            if (killerID === undefined) {
               killerID = req.user.killerID;
            }
            const userInfo =  (await db.query(`SELECT * FROM public."User" 
                    INNER JOIN public."KillerUser" ON public."KillerUser"."UserID" = public."User"."UserID" AND 
                        public."KillerUser"."KillerUserID" = ${killerID}`)).rows[0];

            if (userInfo === undefined) {
                return res.status(404).json({message: "Пользователь не найден"});
            }

            userInfo.level = levelSystem(userInfo.experience);

            const expNextLvl = 50 * (2 ** (userInfo.level - 1));
            let expLvl = expNextLvl / 2; 
            if (userInfo.level == 1) {
                expLvl = 0;
            }
            let expToLvl = expNextLvl - expLvl;
        
            let needExpToNextLvl = expNextLvl - userInfo.experience;

            userInfo.progressLvlPercent = 100 - (needExpToNextLvl / (expToLvl / 100));
          

            return res.status(200).json(userInfo);
        }
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }
    }
}
const authController = new AuthController();
module.exports = authController;


