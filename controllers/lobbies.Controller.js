const db = require('../db');
const jwt = require('jsonwebtoken');
const {secret} = require('../config');
const lobbieCondition = require('../utils/lobbyCondition');
const levelSystem = require('../utils/levelSystem');
const playerStatus = require('../utils/playerStatus');
let Achievements = require('../utils/achievements');

class lobbiesController {

  
    async postLobbie(req, res) {

        try {
            const lobbie = req.body;
            const status = 0;
            const playersCount = 1;
            let password = lobbie.password;
            let LobbyID =  await db.query(`INSERT INTO public."Lobby" ("CreatorID", title, password, status, "playersCount", 
                        "game", "maxCount", "alivePlayers") VALUES ('${req.user.killerID}', '${lobbie.title}',
                          '${password}','${status}', '${playersCount}', '${lobbie.game}', 
                              ${lobbie.maxCount}, ${playersCount}) RETURNING "LobbyID"`);
            if (LobbyID.rowCount == 0) {
                return res.status(404).json({message : "Лобби не создано!"});
            }

            LobbyID = LobbyID.rows[0].LobbyID;

            await db.query(`INSERT INTO public."KillerGame" ("LobbyID", "KillerUserID") VALUES ('${LobbyID}', '${req.user.killerID}')`);

            return res.status(200).json({lobbyID: LobbyID});


        }
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }

    };


    async lobbies(req, res) {

        try {

            const query = req.query;
            if (query.lobbyID === undefined) {
                query.lobbyID = 0;
            }

            let lobbies;
            if (query.UserID === undefined) {
                 lobbies = await db.query(`SELECT * FROM public."Lobby" WHERE  (${!query.lobbyID} OR "LobbyID" = '${query.lobbyID}') AND 
                (${!query.gameTitle} OR "game" = '${query.gameTitle}')`);
            } else {
                const lobbiesIdJson=  (await db.query(`SELECT "LobbyID" FROM public."KillerGame" WHERE "KillerUserID" = ${query.UserID}`)).rows;
                const lobbiesID = lobbiesIdJson.map(function(lobby) {
                    return lobby.LobbyID;
                })
                if (lobbiesID.length == 0) {
                   return res.status(404).json({message : "Данный игрок не состоит ни в 1 лобби"});
                }
                lobbies = await db.query(`SELECT * FROM public."Lobby" WHERE  (${!query.lobbyID} OR "LobbyID" = '${query.lobbyID}') AND 
                (${!query.gameTitle} OR "game" = '${query.gameTitle}') AND ("LobbyID" IN (${lobbiesID}))`);
            }

            let usernames = (await db.query(`SELECT login, "KillerUserID" FROM public."User"
                INNER JOIN public."KillerUser" ON public."KillerUser"."UserID" = public."User"."UserID"`)).rows;
            lobbies.rows.forEach(function(lobby, i, lobbies) {
                let creatorName;
                let j = 0;
                while (usernames[j].KillerUserID != lobby.CreatorID) {
                    j++;
                }
                lobby.creator = usernames[j].login;
                lobby.isCreator = (req.user.killerID == lobby.CreatorID);

            })
            return res.status(200).json(lobbies.rows);
        }
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }

    };


    async entrance(req, res) {
        try {
            const lobby = req.body;
            let searchedLobby = await db.query(`SELECT * FROM public."Lobby" WHERE "LobbyID" = ${lobby.lobbyID}`)
            if (searchedLobby.rowCount == 0) {
               return  res.status(404).json({message : "Лобби не найдено"});
            }
            searchedLobby = searchedLobby.rows[0];
            if ((searchedLobby.password != "")  && (searchedLobby.password != lobby.passwordLobby)) {
               return  res.status(403).json({message : "Пароль от лобби неверный"});
            }
            if (searchedLobby.status != lobbieCondition.WAITING || searchedLobby.playersCount == searchedLobby.maxCount) {
               return  res.status(406).json({message : "В данное лобби невозможно присоединиться"});
            }

            const candidate = await db.query(`SELECT "KillerUserID" FROM public."KillerGame" 
                                        WHERE "KillerUserID" = ${req.user.killerID} AND "LobbyID" = ${lobby.lobbyID}`);
            if (candidate.rowCount != 0) {
               return  res.status(409).json({message : "Вы уже состоите в этом лобби!"});
            }
            
            await db.query(`INSERT INTO public."KillerGame" ("LobbyID", "KillerUserID") VALUES ('${lobby.lobbyID}', '${req.user.killerID}')`);

            await db.query(`UPDATE public."Lobby" SET "playersCount" = ${++searchedLobby.playersCount}, 
                            "alivePlayers" = ${++searchedLobby.alivePlayers} WHERE "LobbyID" = ${searchedLobby.LobbyID}`);
           return res.status(200).json({message : "Успешно добавлен"});

        }
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }
    };

    async killerMembers(req, res) {
        try {
            const lobbyID = req.query.lobbyID;
            let members = (await db.query(`SELECT public."KillerGame"."KillerUserID", "login", "avatar", "experience", "OpponentID",
             "playerStatus", public."KillerGame"."kills" FROM public."KillerGame" 
                INNER JOIN public."KillerUser" ON public."KillerGame"."LobbyID" = ${lobbyID} AND
                    public."KillerUser"."KillerUserID" =  public."KillerGame"."KillerUserID" 
                INNER JOIN public."User" ON   public."User"."UserID" = public."KillerUser"."UserID"`)).rows;

            members.forEach((member) => {
                member.level = levelSystem(member.experience);
                // if (member.startDate != null && member.playerStatus == playerStatus.ALIVE) {
                //     let mseconds = (new Date().getTime() -  member.startDate.getTime());
                //     member.hoursAlive = (mseconds / (1000 * 60 * 60));   // тестить время! Сейчас null
                //     console.log(member.startDate);
                //     console.log(member.hoursAlive);
                // }

            });
                
            return res.status(200).json(members);

        }
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }
    };



    async startKiller(req, res) {
        try {
            let lobby =  (await db.query(`SELECT * FROM public."Lobby" WHERE  "LobbyID" = ${req.body.lobbyID}`));
            if (lobby.rowCount == 0) {
                return res.status(404).json({message : "Лобби не существует!"});
            }
            lobby = lobby.rows[0];
            if (req.user.killerID != lobby.CreatorID) {
                return res.status(403).json({message : "Невозможно начать игру, вы не создатель лобби"});
            }
            if (lobby.playersCount < 4) {
                return res.status(406).json({message : "Минимальное количество игроков - 4"});
            }

            let players = (await db.query(`SELECT "KillerUserID" FROM public."KillerGame" 
            WHERE "LobbyID" = ${req.body.lobbyID}`)).rows;

            players = players.map((player) => {
                return player.KillerUserID;
            });
            console.log(players);

            players = players.map(i=>[Math.random(), i]).sort().map(i=>i[1])
            let pairs = [];
            let i = 0;
            for (i = 0; i < players.length - 1; i++) {
                pairs[i] = {
                    playerID : players[i],
                    opponentID : players[i + 1]
                };
            }
            pairs[i] = {
                playerID : players[i],
                opponentID : players[0]
            };
    
            let date = new Date().toJSON();
            
            for (let i = 0; i < pairs.length; i++) {
                await db.query(`UPDATE public."KillerGame" SET "OpponentID" = ${pairs[i].opponentID},
                     "contractDate" = '${date}',
                    "playerStatus" = ${playerStatus.ALIVE}, kills = ${0}
                    WHERE "KillerUserID" = ${pairs[i].playerID}`);
            }
            await db.query(`UPDATE public."Lobby" SET "status" = ${lobbieCondition.ACTIVE}, "startDate" = '${date}'
                            WHERE "LobbyID" = ${req.body.lobbyID}`);


            return res.status(200).json({message : "Игра начата!"});              
        }
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }
    };

    async died(req, res) {
        try {
            // проверить что лобби активно!
            const user = req.user;

            const LobbyID = req.body.lobbyID;

            const lobby = (await db.query(`SELECT * FROM public."Lobby" WHERE "LobbyID" = ${LobbyID}`)).rows[0];

            if ( lobby === undefined || lobby.status != lobbieCondition.ACTIVE || lobby.alivePlayers <= 2) {
                 return res.status(426).json({message : "Необходимо обновить лобби!"});
            }   
            
            let deadUser = await db.query(`SELECT * FROM public."KillerGame" WHERE "LobbyID" = ${LobbyID} 
                        AND "KillerUserID" = ${user.killerID} AND "playerStatus" = ${playerStatus.ALIVE}`);
            if (deadUser.rowCount == 0) {
                return res.status(404).json({message : "Данный пользователь не найден или уже мертв"});
            }
            deadUser = deadUser.rows[0];

            let killer = await db.query(`SELECT * FROM public."KillerGame" WHERE "LobbyID" = ${LobbyID} 
            AND "OpponentID" = ${user.killerID} AND "playerStatus" = ${playerStatus.ALIVE}`);

            if (killer.rowCount == 0) {
                return res.status(404).json({message : "Убийца не найден или уже мертв"});
            }
            killer = killer.rows[0];

            let newContractDate = new Date();
            let mseconds = (newContractDate.getTime() -  lobby.startDate.getTime());
            deadUser.TotalAliveHours = (mseconds / (1000 * 60 * 60)); 
            newContractDate = newContractDate.toJSON();

            deadUser.murderID = killer.KillerUserID;
            deadUser.playerStatus = playerStatus.DEAD;
            deadUser.deadDate = newContractDate;

            await db.query(`UPDATE public."KillerGame" SET  "playerStatus" = ${playerStatus.DEAD}, "deadDate" = '${newContractDate}', "OpponentID" = ${null}
            WHERE "LobbyID" = ${LobbyID} AND "KillerUserID" = ${user.killerID} AND "playerStatus" = ${playerStatus.ALIVE}`);
           
            await db.query(`UPDATE public."KillerGame" SET "OpponentID" = ${deadUser.OpponentID}, "contractDate" = '${newContractDate}',
                    "kills" = ${++killer.kills} WHERE "LobbyID" = ${LobbyID}
                    AND "KillerUserID" = ${killer.KillerUserID} AND "playerStatus" = ${playerStatus.ALIVE}`);

            await db.query(`UPDATE public."Lobby" SET "alivePlayers" = ${--lobby.alivePlayers} WHERE "LobbyID" = ${LobbyID}`);


            // Добавить опыт и получение достижений!
            deadUser.globalStats = (await db.query(`UPDATE public."KillerUser" SET "gamesCount" =  "gamesCount" + 1, "defeats" = "defeats" + 1,  
                        "kils" = "kils" + ${deadUser.kills} WHERE "KillerUserID" = ${user.killerID} RETURNING *`)).rows[0];

            let achievements = new Achievements(lobby, deadUser);
            // achievements.checkAchievements();
            return res.status(200).json(deadUser);              
        }
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }
    };


    async killerLeave(req, res) {
        try {
         const LobbyID = req.body.lobbyID;
         let killerID = req.user.killerID

         let lobby  = (await db.query(`SELECT * FROM public."Lobby" WHERE "LobbyID" = ${LobbyID}`)).rows[0];
        //  "CreatorID" = ${killerID}
         if (lobby === undefined) {
             return  res.status(404).json({message : "Лобби не найдено"});
         }
         if (req.query.killerID != undefined) {
             if (killerID == req.query.killerID) {
                return res.status(406).json({message : "Администратор не может покинуть лобби"});
             }
            if (lobby.CreatorID != killerID) {
             return res.status(403).json({message : "Только администратор может выгонять из лобби"});
            }
            killerID = req.query.killerID;    
         } else {
            if (lobby.CreatorID == req.user.killerID) {
                return res.status(406).json({message : "Администратор не может покинуть лобби"});
            }
         }


         let playersCount = lobby.playersCount;

         let alivePlayers = lobby.alivePlayers;
         
         playersCount--;
        //  let kickedPlayer = await db.query(`SELECT * FROM public."KillerGame" WHERE "LobbyID" = ${LobbyID} 
        //  AND "KillerUserID" = ${killerID}`).rows[0];
         let kickedPlayer = await db.query(`DELETE FROM public."KillerGame" WHERE "LobbyID" = ${LobbyID} 
                                AND "KillerUserID" = ${killerID} RETURNING *`);
         if (kickedPlayer.rowCount == 0) {
             return  res.status(404).json({message : "Игрок не найден"});
         }
         kickedPlayer = kickedPlayer.rows[0];
         if (kickedPlayer.playerStatus === playerStatus.ALIVE) {
            //  let killerID = (await db.query(`SELECT KillerUserID FROM public."KillerGame" WHERE "LobbyID" = ${LobbyID} 
            // AND "OpponentID" = ${kickedPlayer.killerID}`)).rows[0].KillerUserID;
             alivePlayers--;
             await db.query(`UPDATE public."KillerGame" SET "OpponentID" = ${kickedPlayer.OpponentID}, "contractDate" = '${new Date().toJSON()}'
                     WHERE "LobbyID" = ${LobbyID} AND "OpponentID" = ${kickedPlayer.KillerUserID}`);

            await db.query(`UPDATE public."KillerUser" SET "gamesCount" =  "gamesCount" + 1, 
                        "defeats" = "defeats" + 1   WHERE "KillerUserID" = ${kickedPlayer.KillerUserID}`);
         }
         await db.query(`UPDATE public."Lobby" SET "alivePlayers" = ${alivePlayers}, "playersCount" = ${playersCount} WHERE "LobbyID" = ${LobbyID}`);
         
         return res.status(200).json(kickedPlayer);

        }
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }
    };




    async killerWinners(req, res) {
        try {
            const LobbyID = req.body.lobbyID;   
       
            let lobby = await db.query(`UPDATE public."Lobby" SET "status" = ${lobbieCondition.CLOSED}, "endDate" = '${new Date().toJSON()}'  
                            WHERE "LobbyID" = ${LobbyID} AND "status" = ${lobbieCondition.ACTIVE} 
                                                    AND "alivePlayers" < 3 RETURNING *`);
            if (lobby.rowCount == 0) {
                return res.status(406).json({message : "Невозможно определить победителей в данный момент времени"});
            }
            lobby = lobby.rows[0];
            lobby.winners = (await db.query(`UPDATE public."KillerGame" SET "playerStatus" = ${playerStatus.WIN}
                     WHERE "LobbyID" = ${LobbyID} AND "playerStatus" = ${playerStatus.ALIVE}
                              RETURNING "KillerUserID", "playerStatus", "kills"`)).rows;
            if ( lobby.winners === undefined) {
                return res.status(404).json({message : "Победителей нет?!"});
            }              
            for (let i = 0; i < lobby.winners.length; i++) {
                // достижения и опыт
                await db.query(`UPDATE public."KillerUser" SET "gamesCount" =  "gamesCount" + 1, "wins" = "wins" + 1,
                                "kils" = "kils" + ${lobby.winners[i].kills} WHERE "KillerUserID" = ${lobby.winners[i].KillerUserID}`);
            }
            return res.status(200).json(lobby);

        }
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }
    };



    async deleteLobby(req, res) {
        try {
            const LobbyID = req.body.lobbyID;
            let killerID = req.user.killerID
            let lobby  = (await db.query(`SELECT "CreatorID" FROM public."Lobby" WHERE "LobbyID" = ${LobbyID}`)).rows[0];
            if (lobby === undefined) {
                return res.status(404).json({message : "Лобби не найдено"});
            }
            if (lobby.CreatorID != killerID) {
                return res.status(403).json({message : "Только создатель лобби может его удалить"});
            }
            await db.query(`DELETE  FROM public."KillerGame" WHERE "LobbyID" = ${LobbyID}`);

            await db.query(`DELETE  FROM public."Lobby" WHERE "LobbyID" = ${LobbyID}`);

            return res.status(200).json({message : "Лобби успешно удалено!"});
        }
         
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }
    };


    
};


module.exports = new lobbiesController();



