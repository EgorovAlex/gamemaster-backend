const db = require('../db');

class achievementsController {
    async getUserAchievements(req, res) {
        try {
            let userAchiev = await db.query(`SELECT *, public."KillerAchievements"."KillerAchievementsID" FROM public."KillerAchievements" 
                         LEFT JOIN public."KillerUser/Achievements" ON "KillerUserID" = ${req.user.killerID} 
                        AND public."KillerUser/Achievements"."KillerAchievementsID" = public."KillerAchievements"."KillerAchievementsID"`);

            if (userAchiev.rowCount == 0) {
                return res.status(404).json({message : "Достижений нет?!"});
            }
            userAchiev = userAchiev.rows;

            return res.status(200).json(userAchiev);       
        }
         
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }
    };


    async getClosedAchievements(req, res) {
        try {

            let achievs = await db.query(`SELECT "KillerAchievementsID" FROM public."KillerAchievements"
                EXCEPT SELECT "KillerAchievementsID" FROM public."KillerUser/Achievements" WHERE "KillerUserID" = ${req.user.killerID}`);
            if (achievs.rowCount == 0) {
                return res.status(404).json({message : "Все достижения выполнены!"});
            } else {
                achievs = achievs.rows;
                achievs = achievs.map(element => {
                    return element.KillerAchievementsID;
                });
            return res.status(200).json(achievs);       
            }
        }
         
        catch(e) {
            console.log(e);
            res.status(400).json({message : "error"});
        }
    };

    // async findClosedAchievements() {
    //     let achievs = await db.query(`SELECT "KillerAchievementsID" FROM public."KillerAchievements"
    //             EXCEPT SELECT "KillerAchievementsID" FROM public."KillerUser/Achievements" WHERE "KillerUserID" = ${req.user.killerID}`);
    //     if (achievs.rowCount == 0) {
    //         return null;
    //     } else {
    //         achievs = achievs.rows;
    //         achievs = achievs.map(element => {
    //             return element.KillerAchievementsID;
    //         });
    //     return achievs;
    // };
};


module.exports = new achievementsController();
