
const calcLevelByExp = (exp) => {
    let level = 1;
    let reqExp = 50;
    while (exp >= reqExp) {
        reqExp*= 2;
        level++;
    }

    return level;
}



module.exports = calcLevelByExp;
