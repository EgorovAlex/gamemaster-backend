
const playerStatus = {
    IN_LOBBY: 0,
    ALIVE : 1,
    WAITING: 2,
    DENIED: 3,
    DEAD: 4,
    WIN: 5
};


module.exports = playerStatus;
