const express = require('express');
const authRouter = require('./routes/auth.Router');
const lobbiesRouter = require('./routes/lobbies.Router');
const achievRouter = require('./routes/achievements.Router');
const cors = require('cors');

const PORT = process.env.PORT || 5000;

const app = express();


const start = () => {
    try {
        app.use(cors());
        app.use(express.json());
        app.use('/gamemaster', authRouter, lobbiesRouter, achievRouter);
        app.listen(PORT, () => console.log(`server started on PORT ${PORT}`));
        
    }
    catch(e) {
        console.log(e); 
    }
}



start();